api = 2
core = 7.x

; Project* modules and their dependencies, set up in Drupal.org style.

;projects[admin_menu][download][type] = "git"
;projects[admin_menu][download][revision] = "7.x-3.0-rc4"

projects[conflict][download][type] = "git"
projects[conflict][download][revision] = "7.x-1.0"

projects[ctools][download][type] = "git"
projects[ctools][download][revision] = "7.x-1.9"

projects[devel][download][type] = "git"
projects[devel][download][revision] = "7.x-1.5"

projects[entity][download][type] = "git"
projects[entity][download][revision] = "7.x-1.6"

projects[entityreference][download][type] = "git"
projects[entityreference][download][revision] = "7.x-1.1"

projects[field_collection][download][type] = "git"
projects[field_collection][download][revision] = "7.x-1.x"

projects[field_extrawidgets][download][type] = "git"
projects[field_extrawidgets][download][revision] = "7.x-1.1"

projects[field_group][download][type] = "git"
projects[field_group][download][revision] = "7.x-1.3"

projects[flag][download][type] = "git"
projects[flag][download][revision] = "7.x-2.1"

projects[flag_tracker][download][type] = "git"
projects[flag_tracker][download][revision] = "7.x-1.x"

projects[machine_name][download][type] = "git"
projects[machine_name][download][revision] = "7.x-1.x"

projects[project][download][type] = "git"
projects[project][download][revision] = "7.x-2.x"

projects[project_dependency][download][type] = "git"
projects[project_dependency][revision] = "7.x-1.0-beta3"

projects[project_issue][download][type] = "git"
projects[project_issue][download][revision] = "7.x-2.x"

projects[nodechanges][download][type] = "git"
projects[nodechanges][download][revision] = "7.x-1.x"

projects[dereference_list][download][type] = "git"
projects[dereference_list][download][revision] = "7.x-1.x"

projects[token][download][type] = "git"
projects[token][download][revision] = "7.x-1.5"

projects[token_formatters][download][type] = "git"
projects[token_formatters][download][revision] = "7.x-1.2"

projects[views][download][type] = "git"
projects[views][download][revision] = "7.x-3.11"
